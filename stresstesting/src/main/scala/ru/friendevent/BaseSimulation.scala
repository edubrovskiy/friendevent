package ru.friendevent

import io.gatling.core.Predef._
import io.gatling.http.Predef._

trait BaseSimulation extends Simulation {

  val httpConf = http.baseURL("https://localhost:9090/api/v1")
}
