package ru.friendevent.onemethod

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import ru.friendevent.BaseSimulation

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Random

class RegisterSimulation extends BaseSimulation {

  def randomEmail = Random.alphanumeric.take(20).mkString + "@foo.com"

  val scn = scenario("RegisterSimulation")
    .exec(http("request_1")
    .post("/register")
    .body(StringBody(session => s"""{ "email": "$randomEmail", "password": "Qwe_123"  }""")).asJSON)

  setUp(scn.inject(rampUsers(1000) over (30 seconds))).protocols(httpConf)
}
