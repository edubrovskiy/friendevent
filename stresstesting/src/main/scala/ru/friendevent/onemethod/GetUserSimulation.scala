package ru.friendevent.onemethod

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import ru.friendevent.BaseSimulation

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Random

class GetUserSimulation extends BaseSimulation {

  def feeder = Iterator.continually(Map("id" -> (Random.nextInt(100) + 1)))

  val scn = scenario("GetUserSimulation")
      .feed(feeder)
      .exec(http("request_1")
      .get("/users/${id}"))

  setUp(scn.inject(rampUsers(3000) over(30 seconds))).protocols(httpConf)
}
