use fe;

ALTER TABLE eventParticipations
ADD CONSTRAINT uq_eventParticipations UNIQUE(eventId, userId);
