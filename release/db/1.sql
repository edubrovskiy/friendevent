create database fe;
use fe;
SET default_storage_engine = MYISAM;

create table `user` (
	`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`email` VARCHAR(128) NOT NULL UNIQUE,
	`passwordHash` TEXT NOT NULL
);

create table `session` (
	`id` VARCHAR(128) NOT NULL PRIMARY KEY,
	`userId` BIGINT UNIQUE,
	`csrfToken` VARCHAR(128) NOT NULL,
	`createdAt` TIMESTAMP NOT NULL,

	FOREIGN KEY (userId)
		REFERENCES user(id)
		ON DELETE CASCADE
);

create table `event` (
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(128) NOT NULL UNIQUE,
	description TEXT,
	lat DOUBLE NOT NULL,
	lng DOUBLE NOT NULL,
	createdBy BIGINT UNIQUE NOT NULL,

	FOREIGN KEY (createdBy)
		REFERENCES user(id)
		ON DELETE RESTRICT
) ENGINE = MYISAM; # to support spatial data

create table eventParticipations (
	eventId BIGINT NOT NULL,
	userId BIGINT NOT NULL,

	FOREIGN KEY (eventId)
		REFERENCES event(id)
		ON DELETE CASCADE,

	FOREIGN KEY (userId)
		REFERENCES user(id)
		ON DELETE CASCADE
);
