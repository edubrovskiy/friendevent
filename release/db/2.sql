use fe;

ALTER TABLE `event`
DROP lat,
DROP lng,
ADD location POINT NOT NULL,
ADD `date` TIMESTAMP NOT NULL,
DROP FOREIGN KEY event_ibfk_1,
DROP INDEX createdBy,
MODIFY createdBy BIGINT NOT NULL,
ADD CONSTRAINT createdBy_fk FOREIGN KEY (createdBy)
	REFERENCES user(id)
	ON DELETE RESTRICT,
ADD SPATIAL INDEX sp_index (location);