use fe;

CREATE TABLE `role` (
	`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` VARCHAR(128) NOT NULL UNIQUE
);

CREATE TABLE users2roles (
	userId BIGINT NOT NULL,
	roleId BIGINT NOT NULL
);

ALTER TABLE users2roles
ADD CONSTRAINT uq_users2roles UNIQUE(userId, roleId);

INSERT INTO role(name) VALUES ("admin");