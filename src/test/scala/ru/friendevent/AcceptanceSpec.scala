package ru.friendevent

import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import ru.friendevent.responses.ErrorResponse
import ru.friendevent.serialization.JsonProtocol._

abstract class AcceptanceSpec extends WordSpec with Matchers with ScalatestRouteTest {

  def errorText = entityAs[ErrorResponse[String]].error
}