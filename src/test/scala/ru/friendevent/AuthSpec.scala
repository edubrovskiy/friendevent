package ru.friendevent

import java.sql.Timestamp

import akka.http.scaladsl.model.{HttpEntity, StatusCodes}
import akka.http.scaladsl.model.headers.Cookie
import akka.http.scaladsl.server.Directive1
import ru.friendevent.responses.ErrorCode._
import ru.friendevent.util.{AuthHelper, DbHelper}
import ru.friendevent.generated.Tables._
import profile.api._
import akka.http.scaladsl.server.Directives._
import ru.friendevent.fake.FakeData

class AuthSpec(session: Directive1[String], user: Directive1[Long])
              (implicit authHelper: AuthHelper, dbHelper: DbHelper, fakeData: FakeData) extends AcceptanceSpec {

  private def createExpiredSession(): String = {
    val sessionId = authHelper.login().sessionId

    val createdAt = Session.filter(_.id === sessionId).map(_.createdat)
    dbHelper.exec(createdAt.update(Timestamp.valueOf("2000-01-01 00:00:00")))

    sessionId
  }

  val route = (path("") & get & session) { _ =>
    complete(StatusCodes.OK)
  }

  "\"session\" directive" should {

    "extract session id" in {
      val extractingRoute = (path("") & get & session) { sessionId =>
        complete(StatusCodes.OK, HttpEntity(sessionId))
      }

      val sessionId = authHelper.login().sessionId

      Get("/").addHeader(Cookie(Constants.sessionIdCookie, sessionId)) ~> extractingRoute ~> check {
        entityAs[String] shouldEqual sessionId
      }
    }

    s"complete with ${StatusCodes.Unauthorized} if session id is not present" in {

      Get("/") ~> route ~> check {
        status shouldEqual StatusCodes.Unauthorized
        errorText shouldEqual NoSessionId.toString
      }

      Get("/").addHeader(Cookie("someOtherCookie" -> "value")) ~> route ~> check {
        status shouldEqual StatusCodes.Unauthorized
        errorText shouldEqual NoSessionId.toString
      }
    }

    s"complete with ${StatusCodes.Unauthorized} if session does not exist" in {

      Get("/").addHeader(Cookie(Constants.sessionIdCookie, "badSession")) ~> route ~> check {
        status shouldEqual StatusCodes.Unauthorized
        errorText shouldEqual BadSession.toString
      }
    }

    s"complete with ${StatusCodes.Unauthorized} if session is expired" in {

      Get("/").addHeader(Cookie(Constants.sessionIdCookie, createExpiredSession())) ~> route ~> check {
        status shouldEqual StatusCodes.Unauthorized
        errorText shouldEqual BadSession.toString
      }
    }

    s"delete expired sessions" in {

      val sessionId = createExpiredSession()

      Get("/").addHeader(Cookie(Constants.sessionIdCookie, sessionId)) ~> route ~> check {
        dbHelper.exec(Session.filter(_.id === sessionId).result.headOption) shouldBe None
      }
    }
  }

  "\"user\" directive" should {

    s"extract user id" in {

      val extractingRoute = (path("") & get & user) { userId =>
        complete(StatusCodes.OK, HttpEntity(userId.toString))
      }

      val userId = fakeData.Users.users.head.id
      val sessionId = authHelper.login().sessionId
      Get("/").addHeader(Cookie(Constants.sessionIdCookie, sessionId)) ~> extractingRoute ~> check {
        status shouldEqual StatusCodes.OK
        entityAs[String] shouldEqual userId.toString
      }
    }
  }
}
