package ru.friendevent.util

import slick.dbio.DBIO
import ru.friendevent.generated.Tables._
import scala.concurrent.Await
import scala.concurrent.duration._

class DbHelper(implicit db: profile.api.Database) {

  def exec[T](action: DBIO[T]): T = Await.result(db.run(action), 2.seconds)
}
