package ru.friendevent.util

import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.headers.Cookie
import ru.friendevent.Constants
import ru.friendevent.fake.FakeData
import ru.friendevent.responses.SessionResponse
import ru.friendevent.service.AuthService

import scala.concurrent.Await
import scala.concurrent.duration._

class AuthHelper(implicit fakeData: FakeData, authService: AuthService) {

  def logInAndGetCookieHeader(): Cookie =
    Cookie(Constants.sessionIdCookie, login().sessionId)

  def login(userId: Long): SessionResponse =
    Await.result(authService.getOrCreateSession(userId), 2.seconds)

  def login(): SessionResponse = login(fakeData.Users.users.head.id)

  implicit class HttpRequestDeco(httpRequest: HttpRequest) {

    def authenticated: HttpRequest = httpRequest.addHeader(logInAndGetCookieHeader())
  }
}
