package ru.friendevent

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{BeforeAndAfterAll, Suites}
import ru.friendevent.config.TestSupport
import ru.friendevent.config.TestSupport._
import ru.friendevent.config.TestSupport.appConfig._

class AcceptanceTests extends Suites(
  new UserSpec(Routes.userRoute),
  new AuthSpec(appConfig.directives.session, appConfig.directives.user),
  new EventSpec(Routes.eventRoute)
) with BeforeAndAfterAll with LazyLogging {

  implicit val ec = TestSupport.executionContext

  override def afterAll() {
    fakeData.clearDb()
    system.terminate()
  }
}
