package ru.friendevent

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.headers.`Set-Cookie`
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import ru.friendevent.fake.FakeData
import ru.friendevent.requests.{LoginRequest, RegistrationRequest}
import ru.friendevent.responses.{SessionResponse, UserResponse, ValidationResponse}
import ru.friendevent.serialization.JsonProtocol._
import ru.friendevent.util.AuthHelper
import ru.friendevent.responses.ErrorCode._

class UserSpec(route: Route)(implicit fakeData: FakeData, authHelper: AuthHelper)
  extends AcceptanceSpec with LazyLogging {

  import authHelper.HttpRequestDeco

  "/users/id" should {

    "return user" in {
      val user = fakeData.Users.users.head
      Get(s"/users/${user.id}").authenticated ~> route ~> check {
        status shouldEqual OK
        entityAs[UserResponse] shouldEqual UserResponse(user.id, user.email)
      }
    }
  }

  "POST /register" should {

    "return session on successful registration" in {

      val request = RegistrationRequest("nagibator98@gmail.com", "Qwe_12345")

      Post(s"/register", request) ~> route ~> check {
        status shouldEqual OK

        header[`Set-Cookie`].isDefined shouldEqual true
        header[`Set-Cookie`].get.cookie.name shouldEqual "sessionid"

        noException should be thrownBy entityAs[SessionResponse]
      }
    }

    "handle duplicate emails" in {
      val alreadyRegistered = fakeData.Users.registrationRequests.head
      Post(s"/register", alreadyRegistered) ~> route ~> check {
        status shouldEqual BadRequest
      }
    }
  }

  "POST /login" should {

    "return session on successful registration" in {
      val request = LoginRequest("nagibator98@gmail.com", "Qwe_12345")
      Post(s"/login", request) ~> route ~> check {
        status shouldEqual OK

        header[`Set-Cookie`].isDefined shouldEqual true
        header[`Set-Cookie`].get.cookie.name shouldEqual "sessionid"

        noException should be thrownBy entityAs[SessionResponse]
      }
    }

    "return bad email when no user with given email was found" in {
      val request = LoginRequest("nonexistent@gmail.com", "Qwe_12345")
      Post(s"/login", request) ~> route ~> check {
        status shouldEqual BadRequest
        errorText shouldEqual WrongPasswordOrBadEmail.toString
      }
    }

    "handle wrong password" in {
      val request = LoginRequest(fakeData.Users.users.head.email, "wrongPassword")
      Post(s"/login", request) ~> route ~> check {
        status shouldEqual BadRequest
        errorText shouldEqual WrongPasswordOrBadEmail.toString
      }
    }
  }

  "POST /register/validate" should {
    "return \"valid\": true for valid requests" in {

      val request = RegistrationRequest("nagibator99@gmail.com", "Qwe_12345")
      Post(s"/register/validate", request) ~> route ~> check {
        status shouldEqual OK

        val response = entityAs[ValidationResponse]
        response.valid shouldEqual true
      }
    }

    "return \"valid\": false when email is already taken" in {

      val request = RegistrationRequest(fakeData.Users.users.head.email, "Qwe_asd123")
      Post(s"/register/validate", request) ~> route ~> check {
        status shouldEqual OK

        val response = entityAs[ValidationResponse]
        response.valid shouldEqual false
        response.fieldErrors.contains("email") shouldEqual true
      }
    }

    "return \"valid\": false on bad password" in {

      val request = RegistrationRequest("nagibator99@gmail.com", "1234")
      Post(s"/register/validate", request) ~> route ~> check {
        status shouldEqual OK

        val response = entityAs[ValidationResponse]
        response.valid shouldEqual false
        response.fieldErrors.contains("password") shouldEqual true
      }
    }
  }
}
