package ru.friendevent

import java.time.LocalDateTime

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import ru.friendevent.fake.FakeData
import ru.friendevent.util.{AuthHelper, DbHelper}
import ru.friendevent.serialization.JsonProtocol._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import ru.friendevent.generated.Tables._
import profile.api._
import ru.friendevent.requests.CreateEventRequest
import ru.friendevent.responses.CreateEventResponse

class EventSpec(route: Route)(implicit fakeData: FakeData, authHelper: AuthHelper,
                              dbHelper: DbHelper) extends AcceptanceSpec {

  import authHelper.HttpRequestDeco

  "POST /events" should {

    "permit only authenticated users" in {
      val req = fakeData.Events.createRequests.head
      Post("/events", req) ~> route ~> check {
        status shouldEqual StatusCodes.Unauthorized
      }
    }

    "create event" in {
      val req = CreateEventRequest(
        name = "POMIJETU",
        description = "maleishee",
        lat = 40.741895,
        lng = -73.989308,
        date = LocalDateTime.now.plusDays(1)
      )

      Post("/events", req).authenticated ~> route ~> check {
        status shouldEqual StatusCodes.OK

        val id = dbHelper.exec(Event.filter(_.name === req.name).map(_.id).result.headOption)
        assert(id.isDefined)

        entityAs[CreateEventResponse] shouldEqual CreateEventResponse(id.get)
      }
    }
  }
}
