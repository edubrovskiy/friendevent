package ru.friendevent.fake

import java.time.LocalDateTime

import com.typesafe.scalalogging.LazyLogging
import ru.friendevent.generated.Tables._
import profile.api._
import ru.friendevent.db.dao.UserDao
import ru.friendevent.requests.{CreateEventRequest, RegistrationRequest}
import ru.friendevent.service.AuthService
import ru.friendevent.util.DbHelper

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._

class FakeData(implicit
               userDao: UserDao,
               authService: AuthService,
               dbHelper: DbHelper,
               ec: ExecutionContext) extends LazyLogging {

  object Users {

    lazy val registrationRequests = Vector(
      RegistrationRequest("admin@gmail.com", "nimdaa"),
      RegistrationRequest("example@gmail.com", "password")
    )

    Await.result(Future.sequence(registrationRequests.map(authService.register)), 2.seconds)

    lazy val users = Await.result(userDao.getAll, 2.seconds).toVector
  }

  object Events {
    lazy val createRequests = Vector(
      CreateEventRequest(
        name = "MK",
        description = "gonna play MK at my place",
        lat = 40.741895,
        lng = -73.989308,
        LocalDateTime.now.plusDays(1)
      )
    )
  }

  def clearDb() {
    logger.info("Cleaning up test db...")
    dbHelper.exec(Eventparticipations.delete andThen Event.delete andThen Session.delete andThen User.delete)
  }
}
