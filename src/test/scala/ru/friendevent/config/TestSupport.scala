package ru.friendevent.config

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory

object TestSupport {

  implicit val conf = ConfigFactory.load()

  implicit val system = ActorSystem()
  implicit val executionContext = system.dispatcher

  val appConfig = new TestConfig
}
