package ru.friendevent.config

import akka.actor.ActorSystem
import com.typesafe.config.Config
import ru.friendevent.fake.FakeData
import ru.friendevent.util.{AuthHelper, DbHelper}

class TestConfig(implicit actorSystem: ActorSystem, conf: Config) extends AppConfig {

  // To not accidentally use real db in tests.
  require(
    conf.getString("db.url").contains(s"/fe_test?"),
    "Not using test schema!"
  )

  implicit lazy val dbHelper = new DbHelper

  implicit lazy val fakeData = new FakeData
  implicit lazy val authHelper = new AuthHelper
}
