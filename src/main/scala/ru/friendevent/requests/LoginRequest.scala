package ru.friendevent.requests

case class LoginRequest(email: String, password: String)
