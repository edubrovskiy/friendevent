package ru.friendevent.requests

case class Rectangle(minLat: Double, maxLat: Double, minLng: Double, maxLng: Double)
