package ru.friendevent.requests

case class UpdateUserRequest(name: Option[String], info: Option[String])
