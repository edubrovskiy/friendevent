package ru.friendevent.requests

case class RegistrationRequest(email: String, password: String)
