package ru.friendevent.requests

import java.time.LocalDateTime

case class CreateEventRequest(name: String,
                              description: String,
                              lat: Double,
                              lng: Double,
                              date: LocalDateTime)
