package ru.friendevent.serialization

import java.time.LocalDateTime

import ru.friendevent.responses.{ErrorResponse, _}
import ru.friendevent.requests.{CreateEventRequest, LoginRequest, RegistrationRequest, UpdateUserRequest}
import spray.json._

object JsonProtocol extends DefaultJsonProtocol {

  implicit object ldtFormat extends JsonFormat[LocalDateTime] {
    override def write(ldt: LocalDateTime): JsValue =
      JsString(ldt.toString)

    override def read(json: JsValue): LocalDateTime = json match {
      case JsString(s) => LocalDateTime.parse(s)
      case _ => deserializationError("expected string")
    }
  }

  implicit val registrationRequestFormat = jsonFormat2(RegistrationRequest.apply)
  implicit val loginRequestFormat = jsonFormat2(LoginRequest.apply)
  implicit val createEventRequestFormat = jsonFormat5(CreateEventRequest)
  implicit val updateUserRequestFormat = jsonFormat2(UpdateUserRequest)

  implicit val userResponseFormat = jsonFormat4(UserResponse.apply)
  implicit val sessionResponseFormat = jsonFormat2(SessionResponse.apply)
  implicit val createEventResponseFormat = jsonFormat1(CreateEventResponse)
  implicit val eventResponseFormat = jsonFormat7(EventResponse)
  implicit val enrollmentStatusResponseFormat = jsonFormat1[String, EnrollmentStatusResponse](EnrollmentStatusResponse.apply)

  implicit val validationResultFormat = jsonFormat3(ValidationResponse.apply)

  implicit def errorResponseFormat[A : JsonFormat]: RootJsonFormat[ErrorResponse[A]] =
    jsonFormat2[String, Option[A], ErrorResponse[A]](ErrorResponse[A])

  implicit def pagedResponseFormat[A : JsonFormat]: RootJsonFormat[PagedResponse[A]] =
    jsonFormat9(PagedResponse[A])
}
