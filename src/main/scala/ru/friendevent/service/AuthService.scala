package ru.friendevent.service

import ru.friendevent.db.DeletionResult
import ru.friendevent.errors.LoginError
import ru.friendevent.validation.ValidationResult
import ru.friendevent.requests.{LoginRequest, RegistrationRequest}
import ru.friendevent.responses.SessionResponse

import scala.concurrent.Future

trait AuthService {

  def register(registrationRequest: RegistrationRequest): Future[SessionResponse]

  def validate(registration: RegistrationRequest): Future[ValidationResult]

  def login(loginRequest: LoginRequest): Future[Either[LoginError, SessionResponse]]

  def getOrCreateSession(userId: Long): Future[SessionResponse]

  def expiredOrNonexistent(sessionId: String): Future[Boolean]

  def createNewSession(userId: Long): Future[SessionResponse]

  def deleteSessionIfExists(userId: Long): Future[DeletionResult]

  def deleteSessionIfExists(sessionId: String): Future[DeletionResult]

  def findUserIdBySessionId(sessionId: String): Future[Option[Long]]

  def hasAnyOfRoles(userId: Long, roles: String*): Future[Boolean]
}
