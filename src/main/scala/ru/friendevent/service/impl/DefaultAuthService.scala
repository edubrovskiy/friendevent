package ru.friendevent.service.impl

import java.security.SecureRandom
import java.sql.Timestamp
import java.util.Base64

import com.typesafe.scalalogging.LazyLogging
import com.wix.accord
import com.wix.accord.{Descriptions, Failure, RuleViolation, Success}
import org.mindrot.jbcrypt.BCrypt
import ru.friendevent.Constants
import ru.friendevent.db.DbConversions._
import ru.friendevent.db.DeletionResult
import ru.friendevent.db.dao.{SessionDao, UserDao}
import ru.friendevent.errors.LoginError
import ru.friendevent.errors.LoginError._
import ru.friendevent.generated.Tables._
import ru.friendevent.requests.{LoginRequest, RegistrationRequest}
import ru.friendevent.responses.SessionResponse
import ru.friendevent.service.AuthService
import ru.friendevent.validation._
import ru.friendevent.validation.Validators._
import scala.concurrent.{ExecutionContext, Future}

class DefaultAuthService(implicit userDao: UserDao, sessionDao: SessionDao, ec: ExecutionContext)
  extends AuthService with LazyLogging {
  private val sessionIdSize = 256
  private val csrfTokenSize = 256

  private val secureRandom = new SecureRandom

  override def register(registration: RegistrationRequest): Future[SessionResponse] = {
    val passwordHash = BCrypt.hashpw(registration.password, BCrypt.gensalt)
    val user = UserRow(0, registration.email, passwordHash)
    userDao.create(user).flatMap(createNewSession)
  }

  override def validate(registration: RegistrationRequest): Future[ValidationResult] =
    userDao.exists(registration.email).map {
      case true => Failure(Set(RuleViolation(
        value = registration,
        constraint = "user with such email already exists",
        description = Descriptions.Explicit("email"))))
      case false => Success
    }.map { result =>
      accord.validate(registration) and result
    }

  override def login(loginRequest: LoginRequest): Future[Either[LoginError, SessionResponse]] =
    userDao.findByEmail(loginRequest.email).flatMap {
      case Some(user) =>
        if (BCrypt.checkpw(loginRequest.password, user.passwordhash))
          getOrCreateSession(user.id).map(Right.apply)
        else
          Future.successful(Left(WrongPassword))
      case None =>
        Future.successful(Left(UserNotFound))
    }

  override def getOrCreateSession(userId: Long): Future[SessionResponse] =
    sessionDao.findByUserId(userId).flatMap {
      case Some(session) => Future.successful(session)
      case None => createNewSession(userId)
    }

  override def expiredOrNonexistent(sessionId: String): Future[Boolean] =
    sessionDao.findById(sessionId).map {
      case Some(session) => System.currentTimeMillis - session.createdat.getTime >= Constants.sessionExpirationTime
      case None => true
    }

  override def createNewSession(userId: Long): Future[SessionResponse] = {

    def generateSession(userId: Long) = {

      def randomBits(n: Int): String = {
        require(n % 8 == 0)

        val bytes = new Array[Byte](n / 8)
        secureRandom.nextBytes(bytes)
        new String(Base64.getEncoder.encode(bytes))
      }

      SessionRow(randomBits(sessionIdSize), Some(userId), randomBits(csrfTokenSize), new Timestamp(System.currentTimeMillis))
    }

    val session = generateSession(userId)

    logger.debug(s"generated $session")

    for {
      _ <- deleteSessionIfExists(userId)
      _ <- sessionDao.create(session)
    } yield session
  }

  override def deleteSessionIfExists(userId: Long): Future[DeletionResult] =
    sessionDao.deleteByUserIdIfExists(userId)

  override def deleteSessionIfExists(sessionId: String): Future[DeletionResult] =
    sessionDao.deleteByIdIfExists(sessionId)

  override def findUserIdBySessionId(sessionId: String): Future[Option[Long]] =
    sessionDao.findUserIdBySessionId(sessionId)

  override def hasAnyOfRoles(userId: Long, roles: String*): Future[Boolean] =
    userDao.hasAnyOfRoles(userId, roles: _*)
}
