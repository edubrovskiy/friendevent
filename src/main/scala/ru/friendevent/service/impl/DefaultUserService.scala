package ru.friendevent.service.impl

import com.typesafe.scalalogging.LazyLogging
import ru.friendevent.responses.UserResponse
import ru.friendevent.service.UserService

import scala.concurrent.{ExecutionContext, Future}
import ru.friendevent.db.DbConversions._
import ru.friendevent.requests.UpdateUserRequest
import ru.friendevent.validation._
import ru.friendevent.validation.Validators._
import com.wix.accord._
import ru.friendevent.db.dao.UserDao

class DefaultUserService(implicit userDao: UserDao, ec: ExecutionContext) extends UserService with LazyLogging {

  override def findById(id: Long): Future[Option[UserResponse]] =
    userDao.findById(id).map(_.map(userRow2UserResponse))

  override def exists(userId: Long): Future[Boolean] =
    userDao.exists(userId)

  override def getUserCount: Future[Int] =
    userDao.getCount

  override def getUsers(page: Int, pageSize: Int): Future[Seq[UserResponse]] = {
    require(page > 0)
    require(pageSize > 0)

    userDao.getAll(page, pageSize).map(_.map(userRow2UserPreviewResponse))
  }

  override def validateUpdateRequest(id: Long, request: UpdateUserRequest): Future[ValidationResult] =
    userDao.findById(id).map {
      case Some(_) => Success
      case None => Failure(Set(RuleViolation(
        value = id,
        constraint = s"user with id $id doesn't exist",
        description = Descriptions.Explicit("user.id")
      )))
    }.map(_ and validate(request))

  override def update(id: Long, request: UpdateUserRequest): Future[Unit] = {
    logger.debug(s"updating user $id")

    userDao.findById(id).map(_.get).flatMap { user =>
      val newUser = user.copy(
        name = request.name,
        info = request.info
      )
      userDao.insertOrUpdate(newUser)
    }
  }
}
