package ru.friendevent.service.impl

import com.typesafe.scalalogging.LazyLogging
import ru.friendevent.generated.Tables._
import ru.friendevent.requests.{CreateEventRequest, Rectangle}
import ru.friendevent.responses.{CreateEventResponse, EventResponse, UserResponse}
import ru.friendevent.service._
import com.wix.accord._
import ru.friendevent.validation._
import ru.friendevent.validation.Validators._
import ru.friendevent.db.DbConversions._
import ru.friendevent.db.dao.{EventDao, EventParticipationDao}

import scala.concurrent.{ExecutionContext, Future}

class DefaultEventService(implicit
                          eventDao: EventDao,
                          eventParticipationDao: EventParticipationDao,
                          ec: ExecutionContext) extends EventService with LazyLogging {

  override def create(request: CreateEventRequest, userId: Long): Future[CreateEventResponse] = {
    val event = EventRow(
      id = 0,
      name = request.name,
      description = Some(request.description),
      location = (request.lat, request.lng),
      createdby = userId,
      date = request.date
    )

    eventDao.create(event).map { id =>
      CreateEventResponse(id)
    }
  }

  override def validateCreateRequest(event: CreateEventRequest): Future[ValidationResult] =
    validate(event) match {
      case Success => eventDao.exists(event.name).map {
        case true => Failure(Set(RuleViolation(
          value = event.name,
          constraint = "event with such name already exists",
          description = Descriptions.Explicit("name"))))
        case false => Success
      }
      case f: Failure => Future.successful(f)
    }

  override def findByRectangle(rectangle: Rectangle, page: Int, pageSize: Int): Future[Seq[EventResponse]] =
    eventDao.findByRectangle(rectangle, page, pageSize).map(_.map(eventRow2EventPreviewResponse))

  override def findById(id: Long): Future[Option[EventResponse]] =
    eventDao.findById(id).map(_.map(eventRow2EventResponse))

  override def getCount(rectangle: Rectangle): Future[Int] =
    eventDao.getCount(rectangle)

  override def enroll(eventId: Long, userId: Long): Future[EnrollmentResult] =
    eventDao.exists(eventId).flatMap {
      case false => Future.successful(EnrollmentResult.Failure(EnrollmentError.EventNotFound))
      case true => eventParticipationDao.exists(eventId, userId).flatMap {
        case true => Future.successful(EnrollmentResult.Failure(EnrollmentError.AlreadyEnrolled))
        case false => eventParticipationDao.enroll(eventId, userId).map(_ => EnrollmentResult.Success)
      }
    }

  override def unenroll(eventId: Long, userId: Long): Future[UnenrollResult] =
    eventDao.exists(eventId).flatMap {
      case false => Future.successful(UnenrollResult.Failure(UnenrollError.EventNotFound))
      case true => eventParticipationDao.exists(eventId, userId).flatMap {
        case false => Future.successful(UnenrollResult.Failure(UnenrollError.NotEnrolled))
        case true => eventParticipationDao.unenroll(eventId, userId).map(_ => UnenrollResult.Success)
      }
    }

  override def getParticipants(eventId: Long, page: Int, pageSize: Int): Future[Seq[UserResponse]] =
    eventParticipationDao.getParticipants(eventId, page, pageSize).map { rows =>
      rows.map(userRow2UserPreviewResponse)
    }

  override def getEnrollments(userId: Long): Future[Seq[EventResponse]] =
    eventParticipationDao.getEnrollments(userId).map { rows =>
      rows.map(eventRow2EventPreviewResponse)
    }

  override def getParticipantCount(eventId: Long): Future[Int] =
    eventParticipationDao.getParticipantCount(eventId)

  override def exists(eventId: Long): Future[Boolean] =
    eventDao.exists(eventId)

  override def isEnrolled(eventId: Long, userId: Long): Future[Boolean] =
    eventParticipationDao.exists(eventId, userId)
}
