package ru.friendevent.service

sealed trait EnrollmentResult

object EnrollmentResult {
  case object Success extends EnrollmentResult
  case class Failure(error: EnrollmentError) extends EnrollmentResult
}
