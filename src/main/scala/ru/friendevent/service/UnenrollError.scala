package ru.friendevent.service

sealed trait UnenrollError

object UnenrollError {
  case object EventNotFound extends UnenrollError
  case object NotEnrolled extends UnenrollError
}