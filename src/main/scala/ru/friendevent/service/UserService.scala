package ru.friendevent.service

import ru.friendevent.requests.UpdateUserRequest
import ru.friendevent.responses.UserResponse
import ru.friendevent.validation.ValidationResult

import scala.concurrent.Future

trait UserService {

  def findById(id: Long): Future[Option[UserResponse]]

  def exists(userId: Long): Future[Boolean]

  def getUserCount: Future[Int]

  def getUsers(page: Int, pageSize: Int): Future[Seq[UserResponse]]

  def validateUpdateRequest(id: Long, request: UpdateUserRequest): Future[ValidationResult]

  def update(id: Long, request: UpdateUserRequest): Future[Unit]
}
