package ru.friendevent.service

sealed trait UnenrollResult

object UnenrollResult {
  case object Success extends UnenrollResult
  case class Failure(error: UnenrollError) extends UnenrollResult
}
