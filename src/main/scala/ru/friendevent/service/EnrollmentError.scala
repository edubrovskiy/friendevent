package ru.friendevent.service

sealed trait EnrollmentError

object EnrollmentError {
  case object EventNotFound extends EnrollmentError
  case object AlreadyEnrolled extends EnrollmentError
}
