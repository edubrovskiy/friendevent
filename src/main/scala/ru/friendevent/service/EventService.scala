package ru.friendevent.service

import ru.friendevent.db.{DeletionResult, InsertionResult}
import ru.friendevent.requests.{CreateEventRequest, Rectangle}
import ru.friendevent.responses.{CreateEventResponse, EventResponse, UserResponse}
import ru.friendevent.validation.ValidationResult

import scala.concurrent.Future

trait EventService {

  def create(request: CreateEventRequest, userId: Long): Future[CreateEventResponse]

  def validateCreateRequest(request: CreateEventRequest): Future[ValidationResult]

  def getCount(rectangle: Rectangle): Future[Int]

  def findByRectangle(rectangle: Rectangle, page: Int, pageSize: Int): Future[Seq[EventResponse]]

  def findById(id: Long): Future[Option[EventResponse]]

  def enroll(eventId: Long, userId: Long): Future[EnrollmentResult]

  def unenroll(eventId: Long, userId: Long): Future[UnenrollResult]

  def getParticipants(eventId: Long, page: Int, pageSize: Int): Future[Seq[UserResponse]]

  def getParticipantCount(eventId: Long): Future[Int]

  def exists(eventId: Long): Future[Boolean]

  def getEnrollments(userId: Long): Future[Seq[EventResponse]]

  def isEnrolled(eventId: Long, userId: Long): Future[Boolean]
}
