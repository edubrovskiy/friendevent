package ru.friendevent.routes

import akka.http.scaladsl.model.{DateTime, StatusCodes}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.headers.HttpCookie
import akka.http.scaladsl.server.Route
import ru.friendevent.Constants
import ru.friendevent.requests.{LoginRequest, RegistrationRequest, UpdateUserRequest}
import ru.friendevent.responses.{ErrorCode, ErrorResponse, PagedResponse, SessionResponse}
import ru.friendevent.service.{AuthService, EventService, UserService}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import ru.friendevent.directives.FeDirectives
import ru.friendevent.serialization.JsonProtocol._
import ru.friendevent.responses.Conversions._
import ru.friendevent.errors.LoginError

import scala.concurrent.ExecutionContext

object UserRouteFactory {

  def createRoute(implicit
                  directives: FeDirectives,
                  userService: UserService,
                  eventService: EventService,
                  authService: AuthService,
                  ec: ExecutionContext): Route = {

    import directives._

    def getUserImpl(id: Long) = onSuccess(userService.findById(id)) {
      case Some(user) => complete(user)
      case None => complete(NotFound, ErrorResponse(ErrorCode.NotFound))
    }

    def completeWithSession(session: SessionResponse): Route =
      setCookie(HttpCookie(
        Constants.sessionIdCookie,
        session.sessionId,
        path = Some("/"),
        httpOnly = false,
        expires = Some(DateTime.now + Constants.sessionExpirationTime)
      )) {
        complete(session)
      }

    def updateUserImpl(id: Long) =
      entity(as[UpdateUserRequest]) { request =>
        valid(userService.validateUpdateRequest(id, request)) {
          onSuccess(userService.update(id, request)) {
            complete(StatusCodes.OK)
          }
        }
      }

    val currentUser = get {
      path("currentUser") {
        user { userId =>
          getUserImpl(userId)
        }
      }
    }

    val getUser = get {
      path("users" / LongNumber) { userId =>
        getUserImpl(userId)
      }
    }

    val getUsers = get {
      (path("users") & parameters("page" ? 1, "pageSize" ? 100)) { (page, pageSize) =>
        allowedRoles("admin") {
          val result = for (
            usersTotal <- userService.getUserCount;
            users <- userService.getUsers(page, pageSize)
          ) yield PagedResponse(
            entititesTotal = usersTotal,
            page = page,
            pageSize = pageSize,
            formatLink = (page, pageSize) => s"/users?page=$page&pageSize=$pageSize",
            entities = users
          )

          onSuccess(result) { res =>
            complete(res)
          }
        }
      }
    }

    val login = post {
      path("login") {
        entity(as[LoginRequest]) { req =>
          onSuccess(authService.login(req)) {
            case Left(LoginError.WrongPassword) | Left(LoginError.UserNotFound) =>
              complete(BadRequest, ErrorResponse(ErrorCode.WrongPasswordOrBadEmail))
            case Right(session) =>
              completeWithSession(session)
          }
        }
      }
    }

    val register = post {
      path("register") {
        entity(as[RegistrationRequest]) { req =>
          valid(authService.validate(req)) {
            onSuccess(authService.register(req)) { session =>
              completeWithSession(session)
            }
          }
        }
      } ~ path("register" / "validate") {
        entity(as[RegistrationRequest]) { req =>
          onSuccess(authService.validate(req)) { result =>
            complete(result)
          }
        }
      }
    }

    val update = put {
      path("users" / LongNumber) { userId =>
        user { currentUserId =>
          if (userId == currentUserId)
            updateUserImpl(userId)
          else
            onSuccess(authService.hasAnyOfRoles(currentUserId, "admin")) {
              case true => updateUserImpl(userId)
              case false => complete(Unauthorized)
            }
        }
      } ~ path("users" / LongNumber / "validate") { userId =>
        entity(as[UpdateUserRequest]) { request =>
          onSuccess(userService.validateUpdateRequest(userId, request)) { result =>
            complete(result)
          }
        }
      }
    }

    val enrollments = get {
      path("users" / LongNumber / "enrollments") { userId =>
        onSuccess(userService.exists(userId)) {
          case false => complete(NotFound, ErrorResponse(ErrorCode.NotFound, s"User $userId was not found."))
          case true => onSuccess(eventService.getEnrollments(userId)) { events =>
            complete(events)
          }
        }
      }
    }

    currentUser ~
    getUser ~
    getUsers ~
    login ~
    register ~
    update ~
    enrollments
  }
}
