package ru.friendevent.routes

import akka.http.scaladsl.server.Route
import ru.friendevent.requests.{CreateEventRequest, Rectangle}
import ru.friendevent.responses.{EnrollmentStatusResponse, EnrollmentStatus, ErrorCode, ErrorResponse, PagedResponse}
import ru.friendevent.service._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes._
import ru.friendevent.directives.FeDirectives
import ru.friendevent.serialization.JsonProtocol._
import ru.friendevent.responses.Conversions._
import scala.concurrent.ExecutionContext

object EventRouteFactory {

  def createRoute(implicit
                  directives: FeDirectives,
                  eventService: EventService,
                  authService: AuthService,
                  ec: ExecutionContext): Route = {

    import directives._

    val getEvent = get {
      path("events" / LongNumber) { id =>
        onSuccess(eventService.findById(id)) {
          case Some(event) => complete(event)
          case None => complete(NotFound)
        }
      }
    }

    val getEvents = get {
      path("events") {
        parameters("minLat".as[Double], "maxLat".as[Double], "minLng".as[Double], "maxLng".as[Double]).as(Rectangle) { rectangle =>
          parameters("page" ? 1, "pageSize" ? 100) { (page, pageSize) =>
            val result = for {
              total <- eventService.getCount(rectangle)
              events <- eventService.findByRectangle(rectangle, page, pageSize)
            } yield PagedResponse(
              entititesTotal = total,
              page = page,
              pageSize = pageSize,
              formatLink = (page, pageSize) => "/events?" +
                s"minLat=${rectangle.minLat}&" +
                s"maxLat=${rectangle.maxLat}&" +
                s"minLng=${rectangle.minLng}&" +
                s"maxLng=${rectangle.maxLng}&" +
                s"page=$page&pageSize=$pageSize",
              entities = events
            )

            onSuccess(result) { result =>
              complete(result)
            }
          }
        }
      }
    }

    val createEvent = post {
      path("events") {
        user { userId =>
          entity(as[CreateEventRequest]) { req =>
            valid(eventService.validateCreateRequest(req)) {
              onSuccess(eventService.create(req, userId)) { response =>
                complete(response)
              }
            }
          }
        }
      } ~ path("events" / "validate") {
        entity(as[CreateEventRequest]) { req =>
          onSuccess(eventService.validateCreateRequest(req)) { result =>
            complete(result)
          }
        }
      }
    }

    val enroll = post {
      path("events" / LongNumber / "enroll") { eventId =>
        user { userId =>
          onSuccess(eventService.enroll(eventId, userId)) {
            case EnrollmentResult.Success => complete(OK)
            case EnrollmentResult.Failure(reason) => reason match {
              case EnrollmentError.EventNotFound =>
                complete(BadRequest, ErrorResponse(ErrorCode.NotFound, s"Event $eventId was not found."))
              case EnrollmentError.AlreadyEnrolled =>
                complete(BadRequest, ErrorResponse(ErrorCode.AlreadyEnrolled, s"User $userId is already enrolled for event $eventId."))
            }
          }
        }
      }
    }

    val unenroll = delete {
      path("events" / LongNumber / "unenroll") { eventId =>
        user { userId =>
          onSuccess(eventService.unenroll(eventId, userId)) {
            case UnenrollResult.Success => complete(OK)
            case UnenrollResult.Failure(reason) => reason match {
              case UnenrollError.EventNotFound =>
                complete(BadRequest, ErrorResponse(ErrorCode.NotFound, s"Event $eventId was not found."))
              case UnenrollError.NotEnrolled =>
                complete(BadRequest, ErrorResponse(ErrorCode.NotEnrolled, s"User $userId is not enrolled for event $eventId."))
            }
          }
        }
      }
    }

    val participants = get {
      path("events" / LongNumber / "participants") { eventId =>
        onSuccess(eventService.exists(eventId)) {
          case false => complete(NotFound, ErrorResponse(ErrorCode.NotFound, s"Event $eventId was not found."))
          case true => parameters("page" ? 1, "pageSize" ? 100) { (page, pageSize) =>
            val result = for {
              total <- eventService.getParticipantCount(eventId)
              participants <- eventService.getParticipants(eventId, page, pageSize)
            } yield PagedResponse(
              entititesTotal = total,
              page = page,
              pageSize = pageSize,
              formatLink = (page, pageSize) => s"/events/$eventId/participants?page=$page&pageSize=$pageSize",
              entities = participants
            )

            onSuccess(result) { res =>
              complete(res)
            }
          }
        }
      }
    }

    val getEnrollmentStatus = get {
      path("events" / LongNumber / "enrollmentstatus") { eventId =>
        user { userId =>
          onSuccess(eventService.exists(eventId)) {
            case true => onSuccess(eventService.isEnrolled(eventId, userId)) { enrolled =>
              val status = if (enrolled) EnrollmentStatus.Enrolled else EnrollmentStatus.NotEnrolled
              complete(EnrollmentStatusResponse(status))
            }
            case false => complete(BadRequest, ErrorResponse(ErrorCode.EventNotFound, s"event $eventId was not found"))
          }
        }
      }
    }

    createEvent ~
    getEvent ~
    getEvents ~
    enroll ~
    unenroll ~
    participants ~
    getEnrollmentStatus
  }
}
