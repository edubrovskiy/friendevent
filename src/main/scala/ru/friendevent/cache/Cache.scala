package ru.friendevent.cache

import scala.concurrent.Future

trait Cache[K, V] {

  def apply(key: K): Option[V]

  def update(key: K, value: V): Unit

  def deleteIfExists(key: K): Unit

  def readThrough(key: K, load: => Future[Option[V]]): Future[Option[V]]
}
