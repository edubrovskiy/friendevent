package ru.friendevent.cache

import scala.concurrent.{ExecutionContext, Future}

class EhcacheCache[K, V](val cacheImpl: org.ehcache.Cache[K, V])(implicit ec: ExecutionContext) extends Cache[K, V] {

  override def apply(key: K): Option[V] =
    if (cacheImpl.containsKey(key))
      Some(cacheImpl.get(key))
    else
      None

  override def update(key: K, value: V): Unit =
    cacheImpl.put(key, value)

  override def deleteIfExists(key: K): Unit =
    cacheImpl.remove(key)

  override def readThrough(key: K, load: => Future[Option[V]]): Future[Option[V]] = this(key) match {
    case value @ Some(_) => Future.successful(value)
    case None => load.map { maybeValue =>
      maybeValue.foreach(update(key, _))
      maybeValue
    }
  }
}
