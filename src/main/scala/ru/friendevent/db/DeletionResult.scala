package ru.friendevent.db

sealed trait DeletionResult

object DeletionResult {

  case object Unaffected extends DeletionResult
  case class RowsDeleted(count: Int) extends DeletionResult

  def apply(count: Int): DeletionResult =
    if (count == 0)
      Unaffected
    else
      RowsDeleted(count)
}
