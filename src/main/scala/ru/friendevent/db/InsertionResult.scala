package ru.friendevent.db

sealed trait InsertionResult

object InsertionResult {

  case object Unaffected extends InsertionResult
  case class RowsInserted(count: Int) extends InsertionResult

  def apply(count: Int): InsertionResult =
    if (count == 0)
      Unaffected
    else
      RowsInserted(count)
}
