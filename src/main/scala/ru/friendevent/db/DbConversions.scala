package ru.friendevent.db

import java.sql.{Blob, Timestamp}
import java.time.LocalDateTime
import javax.sql.rowset.serial.SerialBlob

import com.vividsolutions.jts.geom.{Coordinate, GeometryFactory}
import com.vividsolutions.jts.io.{WKBReader, WKBWriter}
import ru.friendevent.responses.{EventResponse, SessionResponse, UserResponse}
import ru.friendevent.generated.Tables._

import scala.language.implicitConversions

object DbConversions {

  implicit def sessionRow2SessionResponse(sessionRow: SessionRow): SessionResponse =
    new SessionResponse(sessionRow.id, sessionRow.csrftoken)

  implicit def userRow2UserPreviewResponse(userRow: UserRow): UserResponse =
    UserResponse(
      id = userRow.id,
      email = userRow.email,
      name = userRow.name,
      info = None
    )

  implicit def userRow2UserResponse(userRow: UserRow): UserResponse =
    UserResponse(
      id = userRow.id,
      email = userRow.email,
      name = userRow.name,
      info = userRow.info
    )

  def eventRow2EventPreviewResponse(eventRow: EventRow): EventResponse = {
    val (lat, lng) = blob2LatLng(eventRow.location)
    EventResponse(
      id = eventRow.id,
      name = eventRow.name,
      description = None,
      date = Some(eventRow.date),
      createdBy = Some(eventRow.createdby),
      lat = lat,
      lng = lng
    )
  }

  implicit def eventRow2EventResponse(eventRow: EventRow): EventResponse = {
    val (lat, lng) = blob2LatLng(eventRow.location)
    EventResponse(
      id = eventRow.id,
      name = eventRow.name,
      description = eventRow.description,
      date = Some(eventRow.date),
      createdBy = Some(eventRow.createdby),
      lat = lat,
      lng = lng
    )
  }

  private val wkbReader = new WKBReader
  private val wkbWriter = new WKBWriter
  private val geometryFactory = new GeometryFactory

  implicit def blob2LatLng(blob: Blob): (Double, Double) = {
    val bytes = blob.getBytes(1, blob.length.toInt)

    // first 4 bytes are SRID
    val wkb = bytes.slice(4, bytes.length)
    val geometry = wkbReader.read(wkb)
    val coord = geometry.getCoordinate
    (coord.x, coord.y)
  }

  implicit def LatLng2Blob(latLng: (Double, Double)): Blob = {
    val point = geometryFactory.createPoint(new Coordinate(latLng._1, latLng._2))
    val wkb = wkbWriter.write(point)
    new SerialBlob(wkb)
  }

  implicit def localDateTime2Timestamp(ldt: LocalDateTime): Timestamp = Timestamp.valueOf(ldt)

  implicit def timestamp2LocalDateTime(ts: Timestamp): LocalDateTime = ts.toLocalDateTime
}
