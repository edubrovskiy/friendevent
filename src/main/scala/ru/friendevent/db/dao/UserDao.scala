package ru.friendevent.db.dao

import ru.friendevent.generated.Tables._

import scala.concurrent.Future

trait UserDao {
  def findById(id: Long): Future[Option[UserRow]]

  /**
    * Use version with pagination instead.
    * TODO: use streams or something.
    */
  def getAll: Future[Seq[UserRow]]

  def getCount: Future[Int]

  def getAll(page: Int, pageSize: Int): Future[Seq[UserRow]]

  def create(user: UserRow): Future[Long]

  def findByEmail(email: String): Future[Option[UserRow]]

  def exists(id: Long): Future[Boolean]

  def exists(email: String): Future[Boolean]

  def insertOrUpdate(user: UserRow): Future[Unit]

  def hasAnyOfRoles(userId: Long, roles: String*): Future[Boolean]
}
