package ru.friendevent.db.dao

import ru.friendevent.generated.Tables._
import ru.friendevent.requests.Rectangle

import scala.concurrent.Future

trait EventDao {

  def create(event: EventRow): Future[Long]

  def exists(name: String): Future[Boolean]

  def exists(id: Long): Future[Boolean]

  def findByRectangle(rectangle: Rectangle, page: Int, pageSize: Int): Future[Seq[EventRow]]

  def findById(id: Long): Future[Option[EventRow]]

  def getCount(rectangle: Rectangle): Future[Int]
}
