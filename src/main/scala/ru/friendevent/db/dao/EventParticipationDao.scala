package ru.friendevent.db.dao

import ru.friendevent.db.{DeletionResult, InsertionResult}
import ru.friendevent.generated.Tables.{EventRow, UserRow}

import scala.concurrent.Future

trait EventParticipationDao {

  def enroll(eventId: Long, userId: Long): Future[InsertionResult]

  def unenroll(eventId: Long, userId: Long): Future[DeletionResult]

  def exists(eventId: Long, userId: Long): Future[Boolean]

  def getParticipants(eventId: Long, page: Int, pageSize: Int): Future[Seq[UserRow]]

  def getParticipantCount(eventId: Long): Future[Int]

  def getEnrollments(userId: Long): Future[Seq[EventRow]]
}
