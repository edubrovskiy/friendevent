package ru.friendevent.db.dao

import ru.friendevent.db.{DeletionResult, InsertionResult}
import ru.friendevent.generated.Tables._

import scala.concurrent.Future

trait SessionDao {
  def findById(id: String): Future[Option[SessionRow]]

  def create(session: SessionRow): Future[InsertionResult]

  def findByUserId(userId: Long): Future[Option[SessionRow]]

  def deleteByUserIdIfExists(userId: Long): Future[DeletionResult]

  def deleteByIdIfExists(sessionId: String): Future[DeletionResult]

  def findUserIdBySessionId(sessionId: String): Future[Option[Long]]
}
