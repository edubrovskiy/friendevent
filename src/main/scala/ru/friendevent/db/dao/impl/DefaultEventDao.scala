package ru.friendevent.db.dao.impl

import java.sql.Blob

import ru.friendevent.generated.Tables._
import profile.api._
import ru.friendevent.db.dao.EventDao
import ru.friendevent.requests.Rectangle
import slick.jdbc.{GetResult, PositionedParameters, SetParameter}

import scala.concurrent.{ExecutionContext, Future}

class DefaultEventDao(implicit db: Database, ec: ExecutionContext) extends EventDao {

  private implicit val setBlobParameter = new SetParameter[Blob] {
    override def apply(blob: Blob, pp: PositionedParameters): Unit = pp.setBlob(blob)
  }

  private implicit val getBlob = GetResult(_.nextBlob)

  override def create(event: EventRow): Future[Long] = {
    val action = sqlu"""
                    INSERT INTO `event` (`name`,`description`,`createdBy`,`location`,`date`)
                    VALUES (${event.name}, ${event.description}, ${event.createdby}, PointFromWKB(${event.location}), ${event.date})
        """.flatMap(_ => sql"""SELECT LAST_INSERT_ID()""".as[Long].head).withPinnedSession
    db.run(action)
  }

  override def exists(name: String): Future[Boolean] =
    db.run(Event.filter(_.name === name).exists.result)

  override def exists(id: Long): Future[Boolean] =
    db.run(Event.filter(_.id === id).exists.result)

  override def findByRectangle(rectangle: Rectangle, page: Int, pageSize: Int): Future[Seq[EventRow]] = {
    require(page > 0)
    require(pageSize > 0)

    val action = sql"""
                    SELECT * FROM `event`
                    WHERE MBRContains(ST_GeomFromText('Polygon((
                    	#${rectangle.minLat} #${rectangle.minLng},
                    	#${rectangle.minLat} #${rectangle.maxLng},
                    	#${rectangle.maxLat} #${rectangle.maxLng},
                    	#${rectangle.maxLat} #${rectangle.minLng},
                      #${rectangle.minLat} #${rectangle.minLng}
                    ))'), `location`)
                    LIMIT $pageSize
                    OFFSET ${(page - 1) * pageSize}
      """.as[EventRow]

    db.run(action)
  }

  override def findById(id: Long): Future[Option[EventRow]] =
    db.run(Event.filter(_.id === id).result.headOption)

  override def getCount(rectangle: Rectangle): Future[Int] = {
    val action = sql"""
                    SELECT COUNT(*) FROM `event`
                    WHERE MBRContains(ST_GeomFromText('Polygon((
                    	#${rectangle.minLat} #${rectangle.minLng},
                    	#${rectangle.minLat} #${rectangle.maxLng},
                    	#${rectangle.maxLat} #${rectangle.maxLng},
                    	#${rectangle.maxLat} #${rectangle.minLng},
                      #${rectangle.minLat} #${rectangle.minLng}
                    ))'), `location`)
      """.as[Int].head

    db.run(action)
  }
}
