package ru.friendevent.db.dao.impl

import com.typesafe.scalalogging.LazyLogging
import ru.friendevent.cache.Cache
import ru.friendevent.db.DbUtils.QueryDeco
import ru.friendevent.db.dao.UserDao
import ru.friendevent.generated.Tables._
import ru.friendevent.generated.Tables.profile.api._

import scala.concurrent.{ExecutionContext, Future}

class DefaultUserDao(implicit db: profile.api.Database, cache: Cache[java.lang.Long, UserRow], ec: ExecutionContext) extends UserDao with LazyLogging {

  override def findById(id: Long): Future[Option[UserRow]] =
    cache.readThrough(id, db.run(User.filter(_.id === id).result.headOption))

  override def getAll: Future[Seq[UserRow]] =
    db.run(User.result)

  override def getCount: Future[Int] =
    db.run(User.length.result)

  override def getAll(page: Int, pageSize: Int): Future[Seq[UserRow]] = {
    require(page > 0)
    require(pageSize > 0)

    db.run(User.getPage(page, pageSize).result)
  }

  override def create(user: UserRow): Future[Long] =
    db.run(User.returning(User.map(_.id)) += user)

  override def findByEmail(email: String): Future[Option[UserRow]] =
    db.run(User.filter(_.email === email).result.headOption)

  override def exists(id: Long): Future[Boolean] =
    db.run(User.filter(_.id === id).exists.result)

  override def exists(email: String): Future[Boolean] =
    db.run(User.filter(_.email === email).exists.result)

  override def insertOrUpdate(user: UserRow): Future[Unit] =
    db.run(User.insertOrUpdate(user)).map { _ =>
      logger.debug(s"deleteing user ${user.id} from cache")
      cache.deleteIfExists(java.lang.Long.valueOf(user.id))
    }

  override def hasAnyOfRoles(userId: Long, roles: String*): Future[Boolean] = {
    val roleIds = Role.filter(_.name.inSet(roles)).map(_.id)
    val query = Users2roles.filter(_.userid === userId).filter(_.roleid.in(roleIds)).exists.result
    db.run(query)
  }
}
