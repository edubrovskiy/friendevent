package ru.friendevent.db.dao.impl

import com.typesafe.scalalogging.LazyLogging
import ru.friendevent.db.{DeletionResult, InsertionResult}
import ru.friendevent.generated.Tables._
import profile.api._
import ru.friendevent.db.dao.SessionDao

import scala.concurrent.{ExecutionContext, Future}

class DefaultSessionDao(implicit db: profile.api.Database, ec: ExecutionContext) extends SessionDao with LazyLogging {

  override def findById(id: String): Future[Option[SessionRow]] =
    db.run(Session.filter(_.id === id).result.headOption)

  override def create(session: SessionRow): Future[InsertionResult] =
    db.run(Session += session).map(InsertionResult.apply)

  override def findByUserId(userid: Long): Future[Option[SessionRow]] =
    db.run(Session.filter(_.userid === userid).result.headOption)

  override def deleteByUserIdIfExists(userId: Long): Future[DeletionResult] =
    db.run(Session.filter(_.userid === userId).delete).map(DeletionResult.apply)

  override def deleteByIdIfExists(sessionId: String): Future[DeletionResult] =
    db.run(Session.filter(_.id === sessionId).delete).map(DeletionResult.apply)

  override def findUserIdBySessionId(id: String): Future[Option[Long]] =
    db.run(Session.filter(_.id === id).map(_.userid).result.headOption).map(_.flatten)
}
