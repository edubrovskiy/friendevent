package ru.friendevent.db.dao.impl

import ru.friendevent.db.{DeletionResult, InsertionResult}
import ru.friendevent.db.dao.EventParticipationDao
import ru.friendevent.generated.Tables._
import profile.api._
import ru.friendevent.db.DbUtils.QueryDeco

import scala.concurrent.{ExecutionContext, Future}

class DefaultEventParticipationDao(implicit db: profile.api.Database, ec: ExecutionContext) extends EventParticipationDao {

  override def enroll(eventId: Long, userId: Long): Future[InsertionResult] =
    db.run(Eventparticipations += EventparticipationsRow(eventId, userId)).map(InsertionResult.apply)

  override def unenroll(eventId: Long, userId: Long): Future[DeletionResult] =
    db.run(Eventparticipations.filter(_.eventid === eventId).filter(_.userid === userId).delete).map(DeletionResult.apply)

  override def exists(eventId: Long, userId: Long): Future[Boolean] =
    db.run(Eventparticipations.filter(_.eventid === eventId).filter(_.userid === userId).exists.result)

  override def getParticipants(eventId: Long, page: Int, pageSize: Int): Future[Seq[UserRow]] = {
    val query = Event.filter(_.id === eventId)
      .join(Eventparticipations).on { case (e, ep) => e.id === ep.eventid }
      .join(User).on { case ((e, ep), u) => u.id === ep.userid }
      .map(_._2)
      .getPage(page, pageSize)

    db.run(query.result)
  }

  override def getParticipantCount(eventId: Long): Future[Int] = {
    val query = Event.filter(_.id === eventId)
      .join(Eventparticipations).on { case (e, ep) => e.id === ep.eventid }
      .join(User).on { case ((e, ep), u) => u.id === ep.userid }
      .length

    db.run(query.result)
  }

  override def getEnrollments(userId: Long): Future[Seq[EventRow]] = {
    val query = User.filter(_.id === userId)
      .join(Eventparticipations).on { case (u, ep) => u.id === ep.userid }
      .join(Event).on { case ((u, ep), e) => e.id === ep.eventid }
      .map(_._2)

    db.run(query.result)
  }
}
