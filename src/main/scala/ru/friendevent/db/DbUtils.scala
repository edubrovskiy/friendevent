package ru.friendevent.db

import slick.lifted.Query

import scala.language.higherKinds

object DbUtils {

  implicit class QueryDeco[E, U, C[_]](query: Query[E, U, C]) {

    def getPage(page: Int, pageSize: Int): Query[E, U, C] =
      query.drop((page - 1) * pageSize).take(pageSize)
  }

}
