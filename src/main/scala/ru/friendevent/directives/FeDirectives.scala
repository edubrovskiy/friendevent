package ru.friendevent.directives

import akka.http.scaladsl.model.StatusCode
import akka.http.scaladsl.server._
import ru.friendevent.validation.ValidationResult
import spray.json.RootJsonFormat

import scala.concurrent.Future

trait FeDirectives extends Directives {

  def valid[A](validationFuture: Future[ValidationResult]): Directive0

  def complete[A : RootJsonFormat](code: StatusCode, body: A): StandardRoute

  def session: Directive1[String]

  def optionalUser: Directive1[Option[Long]]

  def user: Directive1[Long]

  def respondWithCorsHeaders: Directive0

  def allowedRoles(roles: String*): Directive0

  def allowedRoles(userId: Long, roles: String*): Directive0
}
