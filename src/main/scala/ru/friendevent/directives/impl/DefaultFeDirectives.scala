package ru.friendevent.directives.impl

import akka.http.scaladsl.model.StatusCode
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.{Directives => AkkaDirectives, _}
import com.typesafe.config.Config
import com.wix.accord.{Failure, Success}
import ru.friendevent.Constants
import ru.friendevent.directives.FeDirectives
import ru.friendevent.responses.Conversions._
import ru.friendevent.responses.ErrorCode
import ru.friendevent.serialization.JsonProtocol._
import ru.friendevent.service.AuthService
import ru.friendevent.validation._
import spray.json.RootJsonFormat
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.headers.HttpCookiePair
import ru.friendevent.responses.ErrorResponse

import scala.concurrent.{ExecutionContext, Future}

class DefaultFeDirectives(implicit authService: AuthService, ec: ExecutionContext, conf: Config) extends FeDirectives {

  override def valid[A](validationFuture: Future[ValidationResult]): Directive0 =
    onSuccess(validationFuture).flatMap {
      case Success => pass
      case Failure(violations) => complete(BadRequest, ErrorResponse(ErrorCode.ValidationError, violations2Map(violations)))
    }

  override def complete[A : RootJsonFormat](code: StatusCode, body: A): StandardRoute =
    AkkaDirectives.complete(code, body)

  override def session: Directive1[String] =
    optionalCookie(Constants.sessionIdCookie).flatMap {
      case None => complete(Unauthorized, ErrorResponse(ErrorCode.NoSessionId, "Request didn't contain session id."))
      case Some(HttpCookiePair(_, sessionId)) =>
        onSuccess(badSession(sessionId)).flatMap {
          case true => complete(Unauthorized, ErrorResponse(ErrorCode.BadSession, "Session is expired or doesn't exist."))
          case false => provide(sessionId)
        }
    }

  private def badSession(sessionId: String): Future[Boolean] =
    authService.expiredOrNonexistent(sessionId).flatMap {
      case true =>
        authService.deleteSessionIfExists(sessionId).map(_ => true)
      case false =>
        Future.successful(false)
    }

  override def user: Directive1[Long] =
    session.flatMap { sessionId =>
      onSuccess(authService.findUserIdBySessionId(sessionId)).flatMap {
        case Some(id) => provide(id)
        case None => complete(Unauthorized, ErrorResponse(ErrorCode.NoUserFoundForSession, "No user found for session."))
      }
    }

  override def respondWithCorsHeaders: Directive0 = {
    import akka.http.scaladsl.model.HttpMethods._
    import akka.http.scaladsl.model.headers._

    val allowOrigin = `Access-Control-Allow-Origin`(conf.getString("server.accessControl.allowOrigin"))
    val allowMethods = `Access-Control-Allow-Methods`(GET, POST, PUT, DELETE)
    val allowHeaders = `Access-Control-Allow-Headers`("Content-Type", "Cookie")
    val allowCredentials = `Access-Control-Allow-Credentials`(true)

    respondWithHeaders(allowOrigin, allowMethods, allowHeaders, allowCredentials)
  }

  override def optionalUser: Directive1[Option[Long]] = {
    optionalCookie(Constants.sessionIdCookie).flatMap {
      case None => provide(None)
      case Some(HttpCookiePair(_, sessionId)) =>
        onSuccess(badSession(sessionId)).flatMap {
          case true => provide(None)
          case false => onSuccess(authService.findUserIdBySessionId(sessionId)).flatMap {
            case Some(id) => provide(Some(id))
            case None => provide(None)
          }
        }
    }
  }

  override def allowedRoles(roles: String*): Directive0 =
    user.flatMap { userId =>
      allowedRoles(userId, roles: _*)
    }

  override def allowedRoles(userId: Long, roles: String*): Directive0 = {
    onSuccess(authService.hasAnyOfRoles(userId, roles: _*)).flatMap {
      case true => pass
      case false => complete(Unauthorized, ErrorResponse(ErrorCode.Unauthorized, s"User $userId doesn't have any of the required roles."))
    }
  }
}
