package ru.friendevent.config

import java.io.FileInputStream
import java.security.{KeyStore, SecureRandom}
import javax.net.ssl.{KeyManagerFactory, SSLContext, TrustManagerFactory}

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpEntity, HttpResponse}
import akka.http.scaladsl.server.RejectionHandler
import akka.http.scaladsl.{ConnectionContext, HttpsConnectionContext}
import com.typesafe.config.Config
import ru.friendevent.responses.ErrorCode.Rejection
import ru.friendevent.responses.ErrorResponse
import ru.friendevent.serialization.JsonProtocol._
import spray.json._

class ServerConfig(implicit actorSystem: ActorSystem, conf: Config) {

  val httpsConnectionContext: HttpsConnectionContext = {
    val keyStore = KeyStore.getInstance(KeyStore.getDefaultType)

    val inputStream = if (conf.hasPath("security.keyStorePath"))
      new FileInputStream(conf.getString("security.keyStorePath"))
    else
      getClass.getClassLoader.getResourceAsStream("server.p12")

    val password = "changeit".toCharArray
    keyStore.load(inputStream, password)

    val keyManagerFactory = KeyManagerFactory.getInstance("SunX509")
    keyManagerFactory.init(keyStore, password)

    val trustManagerFactory = TrustManagerFactory.getInstance("SunX509")
    trustManagerFactory.init(keyStore)

    val sslContext = SSLContext.getInstance("TLS")
    sslContext.init(keyManagerFactory.getKeyManagers, trustManagerFactory.getTrustManagers, new SecureRandom)

    ConnectionContext.https(sslContext)
  }

  implicit def rejectionHandler: RejectionHandler =
    RejectionHandler.default
      .mapRejectionResponse {
        case res @ HttpResponse(_, _, ent: HttpEntity.Strict, _) =>
          val message = ent.data.utf8String
          res.copy(entity = ErrorResponse(Rejection, message).toJson.prettyPrint)

        case x => x
      }
}
