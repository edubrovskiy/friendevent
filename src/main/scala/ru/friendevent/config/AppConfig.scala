package ru.friendevent.config

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes._
import ru.friendevent.db.dao.impl.{DefaultEventDao, DefaultEventParticipationDao, DefaultSessionDao, DefaultUserDao}
import ru.friendevent.routes.{EventRouteFactory, UserRouteFactory}
import ru.friendevent.service.{AuthService, EventService, UserService}
import ru.friendevent.service.impl.{DefaultAuthService, DefaultEventService, DefaultUserService}
import com.typesafe.config.Config
import org.ehcache.config.builders.{CacheConfigurationBuilder, CacheManagerBuilder, ResourcePoolsBuilder}
import ru.friendevent.cache.{Cache, EhcacheCache}
import ru.friendevent.db.dao.{EventDao, EventParticipationDao, SessionDao, UserDao}
import ru.friendevent.directives.FeDirectives
import ru.friendevent.directives.impl.DefaultFeDirectives
import ru.friendevent.generated.Tables._

class AppConfig(implicit actorSystem: ActorSystem, conf: Config) {

  implicit lazy val executionContext = actorSystem.dispatcher
  implicit lazy val db: profile.api.Database = profile.api.Database.forConfig("db")

  implicit lazy val userCache: Cache[java.lang.Long, UserRow] = new EhcacheCache(Caches.userCache)

  implicit lazy val userDao: UserDao = new DefaultUserDao
  implicit lazy val sessionDao: SessionDao = new DefaultSessionDao
  implicit lazy val eventDao: EventDao = new DefaultEventDao
  implicit lazy val eventParticipationDao: EventParticipationDao = new DefaultEventParticipationDao

  implicit lazy val authService: AuthService = new DefaultAuthService
  implicit lazy val userService: UserService = new DefaultUserService
  implicit lazy val eventService: EventService = new DefaultEventService

  implicit lazy val directives: FeDirectives = new DefaultFeDirectives

  object Routes {
    import directives._

    lazy val appRoute = respondWithCorsHeaders {
      options {
        complete(OK)
      } ~ pathPrefix("api" / conf.getString("api.version")) {
        userRoute ~ eventRoute
      }
    }

    lazy val userRoute = UserRouteFactory.createRoute
    lazy val eventRoute = EventRouteFactory.createRoute
  }

  object Caches {

    val cacheManager = CacheManagerBuilder.newCacheManagerBuilder.build
    cacheManager.init()

    val userCache = cacheManager.createCache("user", CacheConfigurationBuilder.newCacheConfigurationBuilder(
      classOf[java.lang.Long], classOf[UserRow], ResourcePoolsBuilder.heap(100)
    ))
  }
}
