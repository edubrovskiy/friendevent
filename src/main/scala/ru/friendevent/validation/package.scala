package ru.friendevent

package object validation {
  // This type alias is used because "Result" can often be ambiguous.
  type ValidationResult = com.wix.accord.Result
}
