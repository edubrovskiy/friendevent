package ru.friendevent.validation

import com.wix.accord.dsl._
import ru.friendevent.requests.CreateEventRequest

trait EventValidators {

  implicit val createEventRequestValidator = validator[CreateEventRequest] { r =>
    r.name as "name" is notEmpty
    r.name.length.as("name.length").should(be <= 256)

    r.description as "description" is notEmpty
    r.description.length as "description" should be <= 9000

    r.lat as "lat" should (be between(-90.0, 90.0))
    r.lng as "lng" should (be between(-180.0, 180.0))
  }
}
