package ru.friendevent.validation

import com.wix.accord.dsl._
import ru.friendevent.requests.{RegistrationRequest, UpdateUserRequest}

trait UserValidators {
  val passwordValidator = validator[String] { p =>
    p as "password" is notEmpty
    p.length as "password" should be >= 6
    p.length as "password" should be <= 256
  }

  implicit val registrationValidator = validator[RegistrationRequest] { r =>
    r.email as "email" is notEmpty
    r.email.length as "email" should be <= 256

    r.password is valid(passwordValidator)
  }

  implicit val updateValidator = validator[UpdateUserRequest] { r =>
    // TODO: add validation
  }
}
