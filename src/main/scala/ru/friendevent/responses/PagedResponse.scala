package ru.friendevent.responses

case class PagedResponse[A](entitiesTotal: Int,
                            page: Int,
                            pageSize: Int,
                            pagesTotal: Int,
                            entities: Seq[A],
                            prevPageLink: Option[String],
                            nextPageLink: Option[String],
                            firstPageLink: String,
                            lastPageLink: String)

object PagedResponse {

  /**
    * @param formatLink function creating link from page number and page size.
    */
  def apply[A](entititesTotal: Int, page: Int, pageSize: Int, formatLink: (Int, Int) => String, entities: Seq[A]) = {
    require(entititesTotal >= 0)
    require(pageSize > 0)
    require(page > 0)

    val pagesTotal = if (entititesTotal % pageSize == 0) entititesTotal / pageSize else entititesTotal / pageSize + 1

    def outOfRange(page: Int) = page < 1 || page > pagesTotal

    val prevPageLink = if (page == 1 || outOfRange(page)) None else Some(formatLink(page - 1, pageSize))
    val nextPageLink = if (page == pagesTotal || outOfRange(page)) None else Some(formatLink(page + 1, pageSize))
    val firstPageLink = formatLink(1, pageSize)
    val lastPageLink = formatLink(pagesTotal, pageSize)

    new PagedResponse[A](
      entititesTotal,
      page,
      pageSize,
      pagesTotal,
      entities,
      prevPageLink,
      nextPageLink,
      firstPageLink,
      lastPageLink)
  }
}
