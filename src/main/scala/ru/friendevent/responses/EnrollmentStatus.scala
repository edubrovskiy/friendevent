package ru.friendevent.responses

sealed trait EnrollmentStatus extends StringCode

object EnrollmentStatus {
  case object Enrolled extends EnrollmentStatus
  case object NotEnrolled extends EnrollmentStatus
}
