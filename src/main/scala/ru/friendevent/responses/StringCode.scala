package ru.friendevent.responses

trait StringCode {

  // Remove "$" since it's prepended for singletons.
  override def toString: String = getClass.getSimpleName.replaceAll("\\$", "")
}
