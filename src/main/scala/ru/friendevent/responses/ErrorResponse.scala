package ru.friendevent.responses

case class ErrorResponse[A](error: String, details: Option[A])

object ErrorResponse {
  def apply[A](code: ErrorCode, details: A) = new ErrorResponse[A](code.toString, Some(details))

  def apply(code: ErrorCode): ErrorResponse[String] = apply(code.toString)

  // We need some type with JsonFormat for error without details. String is a good type.
  def apply(error: String) = new ErrorResponse[String](error, None)
}
