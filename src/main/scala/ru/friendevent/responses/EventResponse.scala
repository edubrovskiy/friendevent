package ru.friendevent.responses

import java.time.LocalDateTime

case class EventResponse(id: Long,
                         name: String,
                         description: Option[String],
                         date: Option[LocalDateTime],
                         createdBy: Option[Long],
                         lat: Double,
                         lng: Double)
