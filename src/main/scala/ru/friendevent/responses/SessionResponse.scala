package ru.friendevent.responses

case class SessionResponse(sessionId: String, csrfToken: String)
