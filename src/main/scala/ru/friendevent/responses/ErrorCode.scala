package ru.friendevent.responses

sealed trait ErrorCode extends StringCode

object ErrorCode {
  case object BadSession extends ErrorCode
  case object NoSessionId extends ErrorCode
  case object NoUserFoundForSession extends ErrorCode
  case object NotFound extends ErrorCode
  case object WrongPasswordOrBadEmail extends ErrorCode
  case object DuplicateEmail extends ErrorCode
  case object DbConflict extends ErrorCode
  case object ValidationError extends ErrorCode
  case object Rejection extends ErrorCode
  case object AlreadyEnrolled extends ErrorCode
  case object NotEnrolled extends ErrorCode
  case object Unauthorized extends ErrorCode
  case object EventNotFound extends ErrorCode
}
