package ru.friendevent.responses

case class EnrollmentStatusResponse(status: String)

object EnrollmentStatusResponse {

  def apply(status: EnrollmentStatus) = new EnrollmentStatusResponse(status.toString)
}
