package ru.friendevent.responses

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import com.wix.accord._
import ru.friendevent.validation.ValidationResult
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCode
import ru.friendevent.serialization.JsonProtocol._
import spray.json.RootJsonFormat

import scala.language.implicitConversions

object Conversions {

  implicit def violations2Map(violations: Set[Violation]): Map[String, String] =
    flattenViolations(violations).toMap

  private def flattenViolations(violations: Set[Violation]): Seq[(String, String)] =
    violations.flatMap {
      case v: RuleViolation => Seq(Descriptions.render(v.description) -> v.constraint)
      case v: GroupViolation => flattenViolations(v.children)
    }.toSeq

  implicit def validationResult2TRM(result: ValidationResult): ToResponseMarshallable = {
    val validationResponse = result match {
      case Success => ValidationResponse.Success
      case Failure(violations) => ValidationResponse.withErrors(violations2Map(violations))
    }
    ToResponseMarshallable(validationResponse)
  }
}
