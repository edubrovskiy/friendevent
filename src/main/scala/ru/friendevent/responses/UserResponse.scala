package ru.friendevent.responses

final case class UserResponse(id: Long,
                              email: String,
                              name: Option[String] = None,
                              info: Option[String] = None)
