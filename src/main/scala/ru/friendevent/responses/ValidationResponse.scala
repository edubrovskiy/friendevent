package ru.friendevent.responses

case class ValidationResponse(valid: Boolean, messages: Seq[String], fieldErrors: Map[String, String])

object ValidationResponse {

  val Success = new ValidationResponse(true, Seq.empty, Map.empty)

  def withErrors(fieldErrors: Map[String, String]) = new ValidationResponse(false, Seq.empty, fieldErrors)
}
