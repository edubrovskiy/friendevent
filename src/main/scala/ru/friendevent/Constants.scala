package ru.friendevent

import scala.concurrent.duration._

object Constants {
  val sessionIdCookie = "sessionid"
  val sessionExpirationTime = 30.days.toMillis
}
