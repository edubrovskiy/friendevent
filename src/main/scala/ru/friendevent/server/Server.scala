package ru.friendevent.server

import java.io.File

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import ru.friendevent.config.{AppConfig, ServerConfig}

object Server extends App with LazyLogging {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  implicit val conf = if (args.isEmpty)
    ConfigFactory.load()
  else
    ConfigFactory.parseFile(new File(args(0)))

  val appConfig = new AppConfig
  val serverConfig = new ServerConfig

  import serverConfig._

  val host = conf.getString("server.host")
  val port = conf.getInt("server.port")

  Http().setDefaultServerHttpContext(httpsConnectionContext)
  Http().bindAndHandle(
    appConfig.Routes.appRoute,
    host,
    port,
    httpsConnectionContext
  )

  logger.info(s"Server is online at port $port.")
}
