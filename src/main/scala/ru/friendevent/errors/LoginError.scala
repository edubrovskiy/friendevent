package ru.friendevent.errors

sealed trait LoginError

object LoginError {
  case object WrongPassword extends LoginError
  case object UserNotFound extends LoginError
}
