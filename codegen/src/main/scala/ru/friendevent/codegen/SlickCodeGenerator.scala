package ru.friendevent.codegen

import slick.codegen.SourceCodeGenerator
import slick.jdbc.MySQLProfile

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object SlickCodeGenerator {

  def main(args: Array[String]) {
    val outputDir = args(0)
    val pkg = args(1)

    val db = MySQLProfile.api.Database.forConfig("db")

    val model = Await.result(db.run(MySQLProfile.createModel(Some(MySQLProfile.defaultTables))), 20.seconds)

    val codegen = new SourceCodeGenerator(model) {

      override def Table = new Table(_) {

        override def TableClass = new TableClass {

          // Generate tables without schema name to use the same code in main app and in tests.
          override def code = {
            val prns = parents.map(" with " + _).mkString("")
            s"""
class $name(_tableTag: Tag) extends Table[$elementType](_tableTag, \"${model.name.table}\")$prns {
  ${indent(body.map(_.mkString("\n")).mkString("\n\n"))}
}
        """.trim()
          }
        }
      }
    }

    codegen.writeToFile(
      profile = "slick.jdbc.MySQLProfile",
      folder = outputDir,
      pkg = pkg
    )
  }
}
