import sbt.Keys._

import scala.language.postfixOps

lazy val root = (project in file("."))
  .settings(scalaSettings)
  .settings(
    name := """friendevent""",
    version := "1.0.0",
    sourceGenerators in Compile += slickCodeGenTask,
    libraryDependencies ++=
      akkaDependencies ++
      loggingDependencies ++
      dbDependencies ++
      Seq(
        "org.mindrot" % "jbcrypt" % "0.3m",
        "com.wix" %% "accord-core" % "0.6.1",
        "com.vividsolutions" % "jts" % "1.13",
        "org.ehcache" % "ehcache" % "3.4.0"
      )
  ).dependsOn(codegen)

lazy val codegen = project
  .settings(scalaSettings)
  .settings(
    name := "friendevent-codegen",
    libraryDependencies ++= loggingDependencies ++ dbDependencies
  )

lazy val stresstesting = project
  .settings(scalaSettings)
  .settings(
    name := "friendevent-stress-testing",
    libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.3.0"
  )

lazy val scalaSettings = Seq(
  scalaVersion := "2.12.1",
  scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked"),
  libraryDependencies ++= Seq(
    "org.scala-lang" % "scala-reflect" % scalaVersion.value,
    "org.scala-lang" % "scala-compiler" % scalaVersion.value % "provided", // Workaround for https://github.com/scala/bug/issues/10171,
    "org.scalatest" %% "scalatest" % "3.0.3" % Test,
    "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % Test
  )
)

lazy val akkaHttpVersion = "10.0.7"
lazy val akkaDependencies = Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test
)

lazy val loggingDependencies = Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
  "ch.qos.logback" % "logback-classic" % "1.1.7"
)

lazy val slickVersion = "3.2.0"
lazy val dbDependencies = Seq(
  "com.typesafe.slick" %% "slick" % slickVersion,
  "com.typesafe.slick" %% "slick-codegen" % slickVersion,
  "mysql" % "mysql-connector-java" % "6.0.6"
)

lazy val acceptanceTests = taskKey[Unit]("Acceptance tests")
acceptanceTests in Test := {
  (testOnly in Test).toTask(" ru.friendevent.AcceptanceTests").value
}

lazy val slickCodeGenTask = taskKey[Seq[File]]("Generate Tables.scala")
slickCodeGenTask := {
  val r = (runner in Compile).value
  val cp = (dependencyClasspath in Compile).value.files
  val log = streams.value.log

  val outputDir = (sourceManaged in Compile).value.toString
  val pkg = "ru.friendevent.generated"
  val args = Array(outputDir, pkg)

  val codegenClass = (mainClass in Compile in codegen).value.get

  toError(r.run(codegenClass, cp, args, log))

  val fileName = outputDir + "/" + pkg.replaceAll("\\.", "/") + "/Tables.scala"
  Seq(new File(fileName))
}

lazy val apiDocs = taskKey[Unit]("Generate API docs from RAML.")
apiDocs := {
  val docsDir = baseDirectory.value / "docs"
  s"raml2html -i $docsDir/v1/api.raml -o $docsDir/api.html" !
}

assemblyMergeStrategy in assembly := {
  case "application.conf" => MergeStrategy.concat
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
